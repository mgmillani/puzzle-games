# Revision history for decrypt-your-fortune

## 0.6.0.0 -- 2023-03-22

* Count common substrings of a certain length a display them.

## 0.3.0.0 -- 2023-03-05

* Improved user interface
    - Resize window.
		- Color decrypted letters which are mapped twice.
		- Format text using pretty printer.

