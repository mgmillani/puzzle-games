with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "decrypt-your-fortune";
  buildInputs = [ cabal-install 
    (haskellPackages.ghcWithPackages (p:
    [ p.array
      p.base
      p.containers
      p.random
      p.hscurses
			p.transformers
      p.fuzzyset
      p.prettyprinter
    ])
    )
  ];
}
