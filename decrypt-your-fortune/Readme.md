# Decrypt your fortune

![Can you decrypt your fortune?](screenshot.png?raw=true "Screenshot of decrypt your fortune")

A crypto puzzle-game where you receive a text encrypted with a [substitution cipher](https://en.wikipedia.org/wiki/Substitution_cipher)
and you need to decrypt it.

## Building

First install `ghc`, `cabal-install`, `fortune` and `ncurses`. Then run

```
cabal run decrypt-your-fortune
```

If you get linker errors, you might need to use [this workaround](https://wiki.archlinux.org/title/Haskell#Problems_with_linking).

If `fortune` is not available in your `PATH` (for example, on Debian), then change the script used to generate the messages (as described in the *Configuration* section below) to use the correct path.

## Gameplay

On the left side of the screen you can see the frequency of each encrypted character
and also your guess of the character it is mapped to.

If you think that, for example, an `A` in the encrypted text is a `B` in the original text,
first type `A` then `B`.

When you guess the cypher correctly, the original messages will be revealed to you.

In case you need help, you can press `<F5>` to reveal one substitution.

## Configuration

The script `~/.config/decrypt-your-fortune/generate` is used to generate each message (the exact path will depend on your system's configuration).
A sample script is provided on the first execution.
