{ mkDerivation, array, base, containers, directory, filepath
, hscurses, lib, prettyprinter, process, random, text, transformers
, unix
}:
mkDerivation {
  pname = "decrypt-your-fortune";
  version = "0.6.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    array base containers directory filepath hscurses prettyprinter
    process random text transformers unix
  ];
  license = lib.licenses.gpl3Only;
  mainProgram = "decrypt-your-fortune";
}
