module DecryptYourFortune.Entities where

import qualified Data.Array as A

newtype PermutationCypher = PermutationCypher (A.Array Char Char)
newtype Point = Point (Int, Int) deriving (Show, Eq)

data Frequency 
  = VeryFrequent
  | Frequent
  | Common
  | Uncommon
  | Rare
  | Never
  | Neutral
  deriving (Eq, Show, Ord)

data Command = 
  ChangePermutation Char Char
  | Hint
  | Resize
  | Quit
  | ScrollText Int
