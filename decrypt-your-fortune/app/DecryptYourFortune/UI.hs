module DecryptYourFortune.UI
        ( endUI
        , animateParagraphs
        , clearText
        , Context(..)
        , drawDecorations
        , getCommand
        , LineStyle(..)
        , resizeScreen
        , scrollText
        , setup
        , TextStyle(..)
        , writeCypher
        , writeFrequency
        , writeMessage
        , writeInfo
        , writeNumberOfHintsUsed
        , writeParagraphs
        )
where

import DecryptYourFortune.UI.Internal
import DecryptYourFortune.UI.LongFrame.Internal
