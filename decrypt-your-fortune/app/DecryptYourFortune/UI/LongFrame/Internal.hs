module DecryptYourFortune.UI.LongFrame.Internal where

import UI.HSCurses.Curses hiding (isLeft, isRight, rs)
import Data.Char
import Data.List
import Control.Monad.Trans.State
import Control.Monad
import Control.Exception
import Control.Monad.IO.Class
import Control.Concurrent.MVar
import Control.Concurrent
import qualified Prettyprinter as Pretty
import qualified Data.Text     as T
import DecryptYourFortune.Entities

data LongFrame = LongFrame
  { parent        :: Window
  , parentSize    :: (Int, Int)
  , parentTopLeft :: Point
  , frame         :: Window
  , frameSize     :: (Int, Int)
  , rightMargin   :: Int
  , content       :: Pretty.SimpleDocStream LineStyle
  , topVisibleRow :: Int
  }

data TextStyle = Encrypted Frequency | Wrong | Decrypted

data LineStyle = Single TextStyle | Multi TextStyle [(T.Text, TextStyle)]

textStyle (Single style) = style
textStyle (Multi style _) = style

minVisibleLines = 1

new :: Window
    -> Point
    -> Int
    -> Int
    -> IO LongFrame
new win topLeft rows columns = do
  let rows' = max 1 (rows - 2)
      margin = 0
      columns' = max 1 (columns - margin)
  pad <- newPad (rows' + 1) (columns' + 1)
  return $ LongFrame
    { parent = win
    , parentSize = (rows, columns)
    , parentTopLeft = topLeft
    , frame  = pad
    , frameSize = (rows', columns')
    , rightMargin = margin
    , content = Pretty.SEmpty
    , topVisibleRow = 0
    }

write :: LongFrame
      -> Pretty.Doc LineStyle
      -> IO LongFrame
write lf doc = do
  execStateT (writeDocWith (writeString) doc >> refreshFrame) lf

refreshFrame :: StateT LongFrame IO ()
refreshFrame = do
  lf <- get
  let Point (r0, c0) = parentTopLeft lf
      linesDisplayed = min ((fst (frameSize lf)) - (topVisibleRow lf)) (fst (parentSize lf) - 2)
  liftIO $ pRefresh (frame lf)
              (topVisibleRow lf) 0
              (r0 + 1) c0
              (r0 + linesDisplayed) (c0 + snd (frameSize lf) - 1)
  
writeDocWith :: (String -> StateT LongFrame IO ())
             -> Pretty.Doc LineStyle
             -> StateT LongFrame IO ()
writeDocWith writeF doc = do
  lf <- get
  let (h, w) = frameSize lf
      textWidth = w
      prettyDoc = (Pretty.layoutPretty 
                                Pretty.LayoutOptions
                                  { Pretty.layoutPageWidth = Pretty.AvailablePerLine textWidth 1.0}
                  ) doc
  resizeToStream prettyDoc
  lf' <- get
  liftIO $ do
    wMove (frame lf') 0 0
  writeSimpleDocStreamWith writeF prettyDoc

writeStream :: LongFrame
            -> Pretty.SimpleDocStream LineStyle
            -> IO LongFrame
writeStream lf sds = execStateT (writeSimpleDocStreamWith writeString sds ) lf

animate :: LongFrame
        -> Int
        -> Pretty.Doc LineStyle
        -> IO LongFrame
animate lf delay sds = execStateT (writeDocWith (animateString delay) sds) lf

writeSimpleDocStreamWith :: (String -> StateT LongFrame IO ())
                         -> Pretty.SimpleDocStream LineStyle
                         -> StateT LongFrame IO ()
writeSimpleDocStreamWith writeF doc' = do
  write' [Single (Encrypted Neutral)] 1 doc'
  where
    writeMultiLine str rest styleStack lineHeight = do
          case styleStack of
            (Multi style0 lns : st') -> do
              lf <- get
              (y,x) <- liftIO $ getYX $ frame lf
              writeF str
              (y1,x1) <- liftIO $ getYX $ frame lf
              forM_ (zip [1..] lns) $ \(i, (l, s)) -> do
                setStyle s
                liftIO $ wMove (frame lf) (y + i) x
                writeF (T.unpack $ T.take (x1 - x) $ l)
              setStyle style0
              liftIO $ wMove (frame lf) y1 x1
              write' (Multi style0 
                            (map (\(txt, style) -> (T.drop (x1 - x) txt, style)) lns)
                     : st')
                     lineHeight
                     rest
            _ -> do
              writeF str
              write' styleStack lineHeight rest
    write' styleStack lineHeight doc = do
      case doc of
        Pretty.SFail -> fail "SimpleDocStream fail :("
        Pretty.SEmpty -> return ()
        Pretty.SChar c rest -> writeMultiLine [c] rest styleStack lineHeight
        Pretty.SText _ txt rest -> writeMultiLine (T.unpack txt) rest styleStack lineHeight
        Pretty.SLine indent rest -> do
          lf <- get
          let win = frame lf
          liftIO $ do
            (r,_) <- getYX win
            wMove win (r + lineHeight) indent
          let lineHeight' =
                case styleStack of
                  (Multi _ lns : _) -> 1 + (length $ filter (\(t,_) -> not $ T.null t) lns)
                  _ -> 1
          write' styleStack lineHeight' rest
        Pretty.SAnnPush style rest -> do
          setStyle (textStyle style)
          let lineHeight' = case style of
                              Single _ -> lineHeight
                              Multi _ lns -> max lineHeight (1 + length lns)
          write' (style : styleStack) lineHeight' rest
        Pretty.SAnnPop rest -> do
          styleStack' <- case styleStack of
            _ : s : stack' -> do
              setStyle (textStyle s)
              return (s : stack')
            _ -> do
              unsetStyle
              return []
          write' styleStack' lineHeight rest

setStyle (Encrypted freq) = do
  lf <- get
  let p = case freq of
            VeryFrequent -> 11
            Frequent     -> 10
            Common       -> 9
            Uncommon     -> 8
            Rare         -> 7
            Never        -> 6
            Neutral      -> 1
  liftIO $ wAttrSet (frame lf) (attr0, (Pair p))

setStyle Wrong = do
  lf <- get
  liftIO $ wAttrSet (frame lf) (attr0, (Pair 3))

setStyle Decrypted = do
  lf <- get
  liftIO $ wAttrSet (frame lf) (attr0, (Pair 2))

unsetStyle = setStyle (Encrypted Neutral)

scroll :: LongFrame
         -> Int
         -> IO LongFrame
scroll lf amount = do
  let topRow' = max 0 $ min (topVisibleRow lf + amount)
                            (fst (frameSize lf) - 1)
      --visibleRows = min ((fst $ frameSize lf) - topRow') (fst $ parentSize lf)
      Point (r0, c0) = parentTopLeft lf
  evalStateT refreshFrame $ lf{topVisibleRow = topRow'}
  --pRefresh (frame lf)
  --     (topRow') (0)
  --     (r0) c0
  --     (visibleRows + r0 - 1)
  --     ((c0 - (rightMargin lf) + (snd $ frameSize lf)))
  wMove (parent lf) r0 (c0 + (snd (frameSize lf) - 1) - 5)
  attrSet attr0 (Pair 5)
  if (topRow' > 0) then do
    attrSet attr0 (Pair 5)
    wAddStr (parent lf) "↑ MORE"
  else do
    attrSet attr0 (Pair 1)
    wAddStr (parent lf) "      "

  wMove (parent lf) (r0 + fst (parentSize lf) - 1) (c0 + (snd (frameSize lf)) - 5)

  if (topRow' <= (fst (frameSize lf)) - (fst (parentSize lf)) ) then do
    attrSet attr0 (Pair 5)
    wAddStr (parent lf) "↓ MORE"    
  else do
    attrSet attr0 (Pair 1)
    wAddStr (parent lf) "      "
  wRefresh (parent lf) 
  return lf{topVisibleRow = topRow'}

resize :: LongFrame
       -> Point
       -> Int
       -> Int
       -> IO LongFrame
resize lf topLeft rows columns = do
  lf1 <- new (parent lf) topLeft rows columns
  let lf2 = lf1{content = content lf}
  lf3 <- execStateT (resizeToStream (content lf2)) lf2
  writeStream lf3 (content lf3)

resizeToStream :: Pretty.SimpleDocStream LineStyle
               -> StateT LongFrame IO ()
resizeToStream sds = do
  let linesNeeded = countDocLines sds
  lf <- get
  when ((fst $ frameSize lf) < linesNeeded) $ do
    pad <- liftIO $ newPad linesNeeded (1 + snd (frameSize lf))
    put $ lf
      { frame = pad
      , frameSize = (linesNeeded, snd $ frameSize lf)
      , content = sds
      }
  
countDocLines :: Pretty.SimpleDocStream LineStyle -> Int
countDocLines sds = 1 + count' [] 1 sds
  where
    count' styleStack lineHeight sds = case sds of
      Pretty.SFail  -> undefined
      Pretty.SEmpty -> 0
      Pretty.SChar c rest      -> (if c == '\n' then 1 else 0) + count' styleStack lineHeight rest
      Pretty.SText _ txt rest  -> count' styleStack lineHeight rest
      Pretty.SLine indent rest -> 
        let lineHeight' = 
              case styleStack of
                (Multi _ lns : _ ) -> 1 + (length lns)
                _ -> 1
        in
        lineHeight + count' styleStack lineHeight' rest
      Pretty.SAnnPush style rest   -> 
        case style of
          Single _     -> count' (style : styleStack) lineHeight rest
          Multi  _ lns -> count' (style : styleStack)
                                 (max lineHeight (1 + length lns))
                                 rest
      Pretty.SAnnPop rest      -> count' (drop 1 styleStack) lineHeight rest

writeString :: String -> StateT LongFrame IO ()
writeString str = do
  lf <- get
  liftIO $ wAddStr (frame lf) str

animateString :: Int -> String -> StateT LongFrame IO ()
animateString delay str = do
  lf <- get
  (y,x) <- liftIO $ getYX $ frame lf
  let Point (r0, c0) = parentTopLeft lf
      (h,w) = parentSize lf
  when (y > (topVisibleRow lf) + h - 3) $ do
    lf' <- liftIO $ scroll lf (y - ((topVisibleRow lf) + h - 4))
    put lf'
    --let lf' = lf{topVisibleRow = y - c0}
    --put lf'
    --liftIO $ pRefresh (frame lf')
    --                  (topVisibleRow lf') 0
    --                  r0 c0
    --                  (r0 + (topVisibleRow lf'  - y))
    --                  (c0 + snd (frameSize lf') - rightMargin lf')
  lf' <- get
  liftIO $ wMove (frame lf') y x
  liftIO $ forM_ str $ \c -> do
    (y1,x1) <- getYX $ frame lf'
    wAddStr (frame lf') [c]
    catch(
      pRefresh (frame lf')
               y1 x1
               (r0 + (y - topVisibleRow lf') + 1)
               (x1 + c0)
               (r0 + (y - topVisibleRow lf') + 1)
               (x1 + c0))
      (\e -> fail $ show (e :: IOException) ++ "," ++ show (y,y1, x1, topVisibleRow lf', r0, c0, parentSize lf', frameSize lf', countDocLines $ content lf'))
    threadDelay delay
