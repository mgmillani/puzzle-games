module DecryptYourFortune.UI.Internal where

import DecryptYourFortune.Entities
import qualified  DecryptYourFortune.UI.LongFrame as LF

import Control.Monad.Trans.State
import Control.Monad
import Control.Monad.IO.Class
import Control.Concurrent.MVar
import Control.Concurrent
-- import Control.Exception
import Data.Char
import UI.HSCurses.Curses hiding (isLeft, isRight, rs)
import UI.HSCurses.CursesHelper
   ( black, green, red, white )
   --( black, blue, cyan, green, magenta, red, white, yellow )
import qualified Prettyprinter             as Pretty
import qualified Data.Text as T
import System.Posix.Signals (installHandler,Handler(..))
import System.Posix.Signals.Exts (sigWINCH)

data GameScreen = GameScreen
  { cypherColumns        :: Int
  , cypherRows           :: Int
  , cypherFrequencyWidth :: Int
  , textFrame            :: LF.LongFrame
  , textBox              :: (Point, Point)
  , infoBox              :: (Point, Point)
  , infoFrame            :: LF.LongFrame
  , statusBox            :: (Point, Point)
  }

data Context = Context
  { mGameScreen :: MVar GameScreen
  , window      :: Window
  , mLock       :: MVar ()
  , mCommand    :: MVar Command
  }


maxTextWidth = 70
maxInfoWidth = 45

endUI = endWin

setup :: Int -> IO Context
setup frequencyWidth = do
  win <- initScr
  -- (h,w) <- scrSize
  lFrame <- LF.new win (Point (0, 0)) 6 6
  iFrame <- LF.new win (Point (0, 0)) 6 6
  keypad win True
  cBreak True
  echo False
  _ <- cursSet CursorInvisible
  startColor
  cols <- colors
  putStrLn $ "Colors = " ++ show cols
  initPair (Pair $ frequencyClassToColor Neutral) white black
  initPair (Pair 2) green black
  initPair (Pair 3) red black
  initPair (Pair 4) black green
  initPair (Pair 5) black white
  initPair (Pair $ frequencyClassToColor Never)        (Color 13) black
  initPair (Pair $ frequencyClassToColor Rare)         (Color 8) black
  initPair (Pair $ frequencyClassToColor Uncommon)     (Color 9) black
  initPair (Pair $ frequencyClassToColor Common)       (Color 10) black
  initPair (Pair $ frequencyClassToColor Frequent)     (Color 11) black
  initPair (Pair $ frequencyClassToColor VeryFrequent) (Color 12) black
  canChange <- canChangeColor
  when canChange $ do
    initColor (Color 8)  $ fromRGB (158, 153,  92)
    initColor (Color 9)  $ fromRGB ( 35, 165,  72)
    initColor (Color 10) $ fromRGB ( 26, 194, 197)
    initColor (Color 11) $ fromRGB (110, 115, 231)
    initColor (Color 12) $ fromRGB (208, 108, 236)
    initColor (Color 13) $ fromRGB (128, 128, 128)
  let screen = GameScreen
                 { cypherFrequencyWidth = frequencyWidth
                 , cypherColumns        = undefined
                 , cypherRows           = undefined
                 , textBox              = undefined
                 , statusBox            = undefined
                 , textFrame            = lFrame
                 , infoFrame            = iFrame
                 , infoBox              = undefined
                 }
  mScreen <- newMVar screen
  lock <- newMVar ()
  command <- newEmptyMVar
  let cx = Context
             { mGameScreen = mScreen
             , window = win
             , mLock = lock
             , mCommand = command
             }
  evalStateT resizeScreen cx
  _ <- installHandler sigWINCH 
        (Catch $ evalStateT (do
          liftIO $ do
            endWin
            refresh
          withScreen' $ \win' _ -> wclear win'
          sendCommand Resize
        )
          cx >> return ()
        )
        Nothing
  _ <- forkIO $ evalStateT handleKeyboard cx >> return ()
  execStateT
    ( do
      withScreen' drawDecorations
    )
    cx

frequencyClassToColor cls = case cls of
  VeryFrequent -> 11
  Frequent     -> 10
  Common       -> 9
  Uncommon     -> 8
  Rare         -> 7
  Never        -> 6
  Neutral      -> 1

fromRGB (r,g,b) = 
  ( conv r
  , conv g
  , conv b
  )
  where
    conv x = (1000 * x) `div` 255

handleKeyboard :: StateT Context IO ()
handleKeyboard = do
  cx <- get
  cmd <- getKeyCommand
  liftIO $ putMVar (mCommand cx) cmd
  handleKeyboard

sendCommand :: Command -> StateT Context IO ()
sendCommand cmd = do
  cx <- get
  liftIO $ putMVar (mCommand cx) cmd

writeFrequency :: Char -> String -> StateT Context IO ()
writeFrequency letter freq = do
  withScreen' $ \win screen -> do
    let (r,c) = frequencyPos screen letter
    wMove win r (c - length freq + 1)
    wAddStr win freq
  return ()

writeCypher :: Char -> Char -> StateT Context IO ()
writeCypher fromLetter toLetter = do
  withScreen' $ \win screen -> do
    let (r,c) = cypherPos screen fromLetter
    wMove win r c
    attrSet attr0 (Pair 2)
    wAddStr win [toLetter]
    attrSet attr0 (Pair $ frequencyClassToColor Neutral)
  return ()

writeParagraphs :: Pretty.Doc LF.LineStyle
                -> StateT Context IO ()
writeParagraphs doc = do
  withScreen $ \_ screen -> do
    frame' <- LF.write (textFrame screen) doc
    return screen{textFrame = frame'}
  -- writeParagraphsWith writeString

writeInfo :: Pretty.Doc LF.LineStyle
          -> StateT Context IO ()
writeInfo doc = do
  withScreen $ \_ screen -> do
    frame' <- LF.write (infoFrame screen) doc
    return screen{infoFrame = frame'}

animateParagraphs :: Int
                  -> Pretty.Doc LF.LineStyle
                  -> StateT Context IO ()
animateParagraphs delay doc = do
  withScreen $ \_ screen -> do
    frame' <- LF.animate (textFrame screen) delay doc
    return screen{textFrame = frame'}

writeString c skip win str = do
  let (ln, rs) = span (/='\n') str
  wAddStr win ln
  case rs of
    '\n':rs' -> do
      (r', _) <- getYX win
      wMove win (r' + skip + 1) c
      writeString c skip win rs'
    [] -> return ()
    _ -> return ()

writeDocWith :: (Window -> String -> IO ()) -> Window -> Int -> Pretty.Doc LF.TextStyle -> IO ()
writeDocWith writeF win width doc = do
  writeSimpleDocStreamWith writeF win $
    Pretty.layoutPretty 
       Pretty.LayoutOptions
       { Pretty.layoutPageWidth = Pretty.AvailablePerLine width 1.0} $
       doc
  
writeSimpleDocStreamWith :: (Window -> String -> IO ())
                         -> Window
                         -> Pretty.SimpleDocStream LF.TextStyle
                         -> IO ()
writeSimpleDocStreamWith writeF win doc' = do
  (_,c0) <- getYX win
  write' c0 [LF.Encrypted Neutral] doc'
  where
    write' c0 styleStack doc = do
      case doc of
        Pretty.SFail -> fail "SimpleDocStream fail :("
        Pretty.SEmpty -> return ()
        Pretty.SChar c rest -> do
          writeF win [c]
          write' c0 styleStack rest
        Pretty.SText _ txt rest -> do
          writeF win (T.unpack txt)
          write' c0 styleStack rest
        Pretty.SLine indent rest -> do
          writeF win "\n"
          (r,_) <- getYX win
          wMove win (r) (c0 + indent)
          write' c0 styleStack rest
        Pretty.SAnnPush style rest -> do
          setStyle win style
          write' c0 (style : styleStack) rest
        Pretty.SAnnPop rest -> do
          styleStack' <- case styleStack of
            _ : s : stack' -> do
              setStyle win s
              return (s : stack')
            _ -> do
              unsetStyle win
              return []
          write' c0 styleStack' rest

countParagraphLines :: Pretty.SimpleDocStream ann -> Int
countParagraphLines sds = case sds of
  Pretty.SFail  -> undefined
  Pretty.SEmpty -> 0
  Pretty.SChar c rest      -> (if c == '\n' then 1 else 0) + countParagraphLines rest
  Pretty.SText _ txt rest  -> 
    (length $ filter (=='\n') $ T.unpack txt) + countParagraphLines rest
  Pretty.SLine _ rest      -> 1 + countParagraphLines rest
  Pretty.SAnnPush _ rest   -> countParagraphLines rest
  Pretty.SAnnPop rest      -> countParagraphLines rest

setStyle win (LF.Encrypted cls) = wAttrSet win (attr0, (Pair $ frequencyClassToColor cls))
setStyle win LF.Wrong     = wAttrSet win (attr0, (Pair 3))
setStyle win LF.Decrypted = wAttrSet win (attr0, (Pair 2))

unsetStyle win = setStyle win (LF.Encrypted Neutral)

writeMessage :: Pretty.Doc LF.TextStyle -> StateT Context IO ()
writeMessage msg = do
  withScreen' $ \win screen -> do
    let (r,c) = messagePos screen
    wMove win r c
    (_,w) <- scrSize
    attrSet attr0 (Pair 1)
    wAddStr win (take (w - c) $ repeat ' ')
    wMove win r c
    writeDocWith (writeString c 0) win 0 msg
  return ()

writeNumberOfHintsUsed :: Int -> StateT Context IO ()
writeNumberOfHintsUsed n = do
  withScreen' $ \w screen -> do 
    let (r,c) = hintPos screen
    wMove w r c
    wAddStr w $ show n
  return ()

resizeScreen :: StateT Context IO ()
resizeScreen = do
  cx <- get
  liftIO $ withMVar (mLock cx) $ \_ -> do
    modifyMVar_ (mGameScreen cx) $ \screen -> do
      (h,w) <- scrSize
      let cols = 1 + (26 `div` (h - 7))
          freqDigits = cypherFrequencyWidth screen
          c0 = 1 + cols * (freqDigits + 1 + 3 + 3) 
          textWidth = min (w - c0 - 15) maxTextWidth
          infoWidth = min (w - (c0 + textWidth) - 3) maxInfoWidth
      lf' <- LF.resize (textFrame screen) 
                       (Point (1, c0))
                       (h - 6) textWidth
      infoF' <- LF.resize (infoFrame screen)
                          (Point (1, c0 + textWidth + 2))
                          (h - 6) infoWidth
      return $ screen
        { cypherColumns = cols
        , cypherRows = 26 `div` cols
        , textBox = (Point (1, c0), Point (h - 6, c0 + textWidth - 1) )
        , textFrame = lf'
        , statusBox = (Point (h - 4, 1), Point (h - 1, w - 1))
        , infoFrame = infoF'
        , infoBox   = (Point (1, c0 + textWidth + 2), Point (h - 6, w - 2))
        }
  withScreen' $ drawDecorations

withScreen :: (Window -> GameScreen -> IO GameScreen) -> StateT Context IO ()
withScreen f = do
  cx <- get
  liftIO $ withMVar (mLock cx) $ \_ -> do
    modifyMVar_ (mGameScreen cx) $ \screen -> do
      screen' <- f (window cx) screen
      wRefresh (window cx)
      return screen'

withScreen' :: (Window -> GameScreen -> IO ()) -> StateT Context IO ()
withScreen' f = do
  cx <- get
  liftIO $ withMVar (mLock cx) $ \_ -> do
    withMVar (mGameScreen cx) $ \screen -> do
      f (window cx) screen
      wRefresh (window cx)

drawDecorations win screen = do
  (h,w) <- scrSize
  let (Point (_, _), Point (tr1, tc1)) = textBox screen
      cypherTableWidth = (cypherFrequencyWidth screen + 5)
  wMove win 0 0
  let cypherTop = "+" ++ (take cypherTableWidth $ repeat '-')
  let hStr = (concat $ take (cypherColumns screen) $ repeat cypherTop) ++ "+-- Decrypt your fortune --"
  wAddStr win (hStr ++ (take (w - (length hStr) - 1) $ repeat '-'))
  forM_ [1..tr1] $ \r -> do
    forM_ [1.. cypherColumns screen] $ \i -> do
      wMove win r (i * (cypherTableWidth + 1))
      wAddStr win "|"
    wMove win r (tc1 + 2)
    wAddStr win "|"
  forM_ ['A'..'Z'] $ \l -> do
    let (r,c) = letterPos screen l
    wMove win r c
    wAddStr win [l]
  let (Point (r0, c0), Point (_, c1)) = statusBox screen
  wMove win (r0 - 1) c0
  wAddStr win (take (c1 - c0 + 1) $ repeat '-')
  wMove win (r0) (c0 + 1)
  wAddStr win "Hints used: 0"
  attrSet attr0 (Pair 4)
  wMove win (h - 1) 0
  let keyStr = "  <F5> Hint   <F10> Quit"
  wAddStr win $ keyStr ++ (take (w - length keyStr - 1) $ repeat  ' ') 
  wMove win 0 0
  -- wAddStr win 
  attrSet attr0 (Pair 1)
  wRefresh win
  return ()

clearText = do
  withScreen' $ \win screen -> do
    let (Point (r0,c0), Point (r1,c1)) = textBox screen
        w = c1 - c0 + 1
        blank = take w $ repeat ' '
    forM_ [r0..r1] $ \r -> do
      wMove win r c0
      wAddStr win blank

getCommand :: StateT Context IO Command
getCommand = do
  cx <- get
  liftIO $ takeMVar $ mCommand cx

getKeyCommand = do
  ch <- liftIO $ getCh
  case ch of
    KeyChar c -> do
      let c' = toUpper c
      if fromEnum c' >= fromEnum 'A' && fromEnum c' <= fromEnum 'Z' then do
        let fromLetter = c'
        withScreen' $ selectLetter fromLetter
        toLetter   <- liftIO $ getLetter
        withScreen' $ deselectLetter fromLetter
        if toLetter /= '\0' then
          return $ ChangePermutation fromLetter toLetter
        else
          getKeyCommand
      else
        getKeyCommand
    KeyF 5    -> return Hint
    KeyF 10   -> return Quit
    KeyResize -> return Resize
    KeyUp     -> return (ScrollText (-1))
    KeyDown   -> return (ScrollText 1)
    _         -> getKeyCommand

cypherPos screen letter = 
  let (r, c) = frequencyPos screen letter
  in (r, c + 4)

letterPos screen letter = 
  let (r, c) = frequencyPos screen letter
  in (r, c + 2)
  
frequencyPos screen letter = 
  let c = fromEnum letter - fromEnum 'A'
      i = c `div` (cypherRows screen)
  in ( (c `mod` (cypherRows screen)) + 1
     , i * (cypherFrequencyWidth screen + 7) + cypherFrequencyWidth screen
     )

hintPos screen = 
  let (Point (r0, c0), _) = statusBox screen
  in (r0, c0 + 13)
  
messagePos screen = 
  let (Point (r0, c0), _) = statusBox screen
  in (r0, c0 + 18)

getLetter = do
  key <- getCh
  case key of
    KeyChar c -> 
      let c' = toUpper c
      in
      if fromEnum c' >= fromEnum 'A' && fromEnum c' <= fromEnum 'Z' then
        return c'
      else
        getLetter
    _ -> return '\0'

selectLetter letter win screen = do
  let (r, c) = cypherPos screen letter
  wMove win r c
  attrSet attr0 (Pair 4)
  wAddStr win "_"
  wRefresh win

deselectLetter letter win screen = do
  let (r, c) = cypherPos screen letter
  wMove win r c
  attrSet attr0 (Pair 1)
  wAddStr win [letter]
  wRefresh win

scrollText :: Int -> StateT Context IO ()
scrollText amount = do
  withScreen $ \_ screen -> do
    let lf = textFrame screen
    lf' <- LF.scroll lf amount
    return screen{textFrame = lf'}
