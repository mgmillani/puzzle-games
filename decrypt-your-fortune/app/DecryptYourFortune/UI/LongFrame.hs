module DecryptYourFortune.UI.LongFrame
        ( new
        , write
        , animate
        , scroll
        , resize
        , LongFrame
        , TextStyle(..)
        , LineStyle(..)
        )
where

import DecryptYourFortune.UI.LongFrame.Internal
