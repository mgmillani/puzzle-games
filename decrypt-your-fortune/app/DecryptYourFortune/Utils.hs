module DecryptYourFortune.Utils where

import qualified Data.Array as A
import DecryptYourFortune.Entities
import Control.Monad.Trans.State
import System.Random

encrypt (PermutationCypher cypher) str = map (\x -> cypher A.! x) str

frequency str =
  A.accumArray
    (+)
    0
    ('A', 'Z') $
      zip str (repeat 1)

randomRange r = do
  g <- get
  let (x, g') = uniformR r g
  put g'
  return x

show2 i
  | i < 10 = ' ' : (show i)
  | otherwise = show i

numberOfDigits n = ceiling $ logBase 10 $ n + 1

mhead (x:xs) = Just x
mhead [] = Nothing
