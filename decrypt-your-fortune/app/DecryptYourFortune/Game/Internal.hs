module DecryptYourFortune.Game.Internal where

import DecryptYourFortune.Entities
import DecryptYourFortune.Utils
import qualified Data.Array as A
import qualified Data.Map as M
import Data.List
import Control.Monad
import Control.Monad.Trans.State
import Data.Char
import System.Random

data Context = Context
  { realCypher              :: PermutationCypher
  , playerCypher            :: PermutationCypher
  , correctCypher           :: PermutationCypher
  , encryptedMessage        :: [String]
  , encryptedFrequency      :: A.Array Char Int
  , encryptedFrequencyClass :: A.Array Char Frequency
  , originalMessage         :: [String]
  , incorrectAmount         :: Int
  , numberOfHints           :: Int
  , frequentSubstrings      :: [ (String, Int) ]
  }

veryFrequent = 0.09
frequent     = 0.05
common       = 0.03
uncommon     = 0.01

freqToClass f
  | f > veryFrequent = VeryFrequent
  | f > frequent = Frequent
  | f > common = Common
  | f > uncommon = Uncommon
  | f > 0 = Rare
  | otherwise = Never

emptyCypher = A.array ('A','Z') $ zip ['A'..'Z'] (repeat '_')

decryptedMessage :: Context -> [String]
decryptedMessage cx = map (encrypt (playerCypher cx)) (encryptedMessage cx)

frequentDecryptedSubstrings :: Context -> [String]
frequentDecryptedSubstrings cx =
  map (\(s,_) -> encrypt (playerCypher cx) s) (frequentSubstrings cx)


setup rawMessage = do
  gen <- newStdGen
  let msg = map (onlyAZ . map toUpper) $ rawMessage
  let cypher@(PermutationCypher cypher') = evalState randomPermutationCypher gen
      encryptedMsg = map (encrypt cypher) msg
      encryptedFreq = frequency $ concat encryptedMsg
      encryptedTotal = fromIntegral $ sum $ A.elems encryptedFreq
      encryptedClass = 
        A.array (A.bounds encryptedFreq) 
          $ map (\(i,f) -> ( i
                           , freqToClass $ (fromIntegral f) / encryptedTotal))
            $ A.assocs encryptedFreq
  let cx = Context{ realCypher = cypher
                  , playerCypher = PermutationCypher emptyCypher
                  , correctCypher = PermutationCypher $ 
                      A.array ('A', 'Z') $ map (\(c,e) -> (e,c)) $ A.assocs cypher'
                  , encryptedMessage = encryptedMsg
                  , encryptedFrequency      = encryptedFreq
                  , encryptedFrequencyClass = encryptedClass
                  , originalMessage = rawMessage
                  , numberOfHints = 0
                  , incorrectAmount = 
                      (length ['A'..'Z']) - 
                      (length $ filter ((==0) . snd) $ A.assocs $ encryptedFreq)
                  , frequentSubstrings = listFrequentSubstrings [2,3,4] encryptedMsg
                  }
  return cx

changePermutation fromC toC = do
  cx <- get
  let PermutationCypher cypher = playerCypher cx
      PermutationCypher cCypher = correctCypher cx
      cypher' = cypher A.// [(fromC, toC)]
      cx' = cx{ playerCypher = PermutationCypher cypher'
              , incorrectAmount = incorrectAmount cx +
                  if (encryptedFrequency cx) A.! fromC == 0 || (cypher A.! fromC) == toC then
                    0
                  else if cCypher A.! fromC == toC then
                    if cypher A.! fromC /= toC then
                      -1
                    else
                      0
                  else if cypher A.! fromC == (cCypher A.! fromC) then
                    1
                  else
                    0 
              }
  put cx'
  return (incorrectAmount cx' == 0)

revealMostFrequent :: State Context (Maybe (Char, Char))
revealMostFrequent = do
  cx <- get
  let freq = frequency (concat $ encryptedMessage cx) :: A.Array Char Int
      PermutationCypher guessedCypher = playerCypher cx
      PermutationCypher cCypher = correctCypher cx
      hint = mostFrequentWrong freq cCypher guessedCypher
  if hint /= '\0' then do
    _ <- changePermutation hint (cCypher A.! hint)
    cx' <- get
    put cx'{numberOfHints = numberOfHints cx' + 1}
    return $ Just (hint, cCypher A.! hint)
  else
    return Nothing
  
mostFrequentWrong freq rightCypher guessedCypher = 
  let (_, c) = foldr 
                (\(f1, c1) (f0, c0) ->
                  if f1 < f0 then
                    (f0, c0)
                  else if (guessedCypher A.! c1 == '_') then
                    (f1, c1)
                  else if guessedCypher A.! c1 /= rightCypher A.! c1 then
                    (f1, c1)
                  else
                    (f0, c0)
                )
                (-1, '\0')
                $ zip (A.elems freq) ['A'..'Z']
  in c


nuples n str =
  if null (drop (n - 1) str) then
    []
  else
    (take n str) : nuples n (drop 1 str)

nuplesCount :: Int -> [String] -> M.Map String Int
nuplesCount n txts = 
  foldr (\w m -> M.insertWith (+) w 1 m) M.empty $ concatMap (nuples n) txts

--frequentNuples :: Int -> Int -> M.Map a Int -> M.Map a Int
--frequentNuples m minF = M.filter (>= minF) m

listFrequentSubstrings :: [Int] -> [String] -> [(String, Int)]
listFrequentSubstrings ns msg = 
  concatMap (\i ->
          take 10 $ sortBy (\a b -> compare (snd b) (snd a)) 
            $ M.assocs (nuplesCount i msg)
      )
      ns
      
randomPermutationCypher =
  fmap (\(xs,_) -> PermutationCypher $ A.array ('A','Z') $ zip ['A'..'Z'] xs) $
    foldM randomize
          ([], A.array ('A','Z') $ zip ['A'..'Z'] ['A'..'Z'])
          ['A'..'Z']
  where
    randomize (xs, unsorted) n = do
      x <- randomRange (n,'Z')
      let x' = unsorted A.! x
      return ( x' : xs, unsorted A.// [(x, unsorted A.! n)])

onlyAZ ('Ü':ss) = 'U':'E':(onlyAZ ss)
onlyAZ ('Ä':ss) = 'A':'E':(onlyAZ ss)
onlyAZ ('Ö':ss) = 'O':'E':(onlyAZ ss)
onlyAZ ('ß':ss) = 'S':'S':(onlyAZ ss)
onlyAZ ('Á':ss) = 'A':(onlyAZ ss)
onlyAZ ('Ã':ss) = 'A':(onlyAZ ss)
onlyAZ ('É':ss) = 'E':(onlyAZ ss)
onlyAZ ('Ê':ss) = 'E':(onlyAZ ss)
onlyAZ ('Í':ss) = 'I':(onlyAZ ss)
onlyAZ ('Ú':ss) = 'U':(onlyAZ ss)
onlyAZ ('Ó':ss) = 'O':(onlyAZ ss)
onlyAZ ('Ô':ss) = 'O':(onlyAZ ss)
onlyAZ ('Õ':ss) = 'O':(onlyAZ ss)
onlyAZ ('Ç':ss) = 'C':(onlyAZ ss)
onlyAZ (s:ss)
  | s >= 'A' && s <= 'Z' =  s:(onlyAZ ss)
  | otherwise = onlyAZ ss
onlyAZ [] = []

