module Main where

import qualified Data.Text     as T
import qualified Data.Array    as A
import qualified Data.Set      as S
import qualified Data.Map      as M
import qualified Prettyprinter as Pretty
import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Data.Char
import System.Directory
import System.Environment
import System.FilePath
import System.IO
import System.Process

import qualified DecryptYourFortune.UI as UI
import qualified DecryptYourFortune.Game as Game
import DecryptYourFortune.Entities
import DecryptYourFortune.Utils

-- identityCypher = A.array ('A','Z') $ zip ['A'..'Z'] ['A'..'Z']

data Universe = Universe
  { uUI :: UI.Context
  , uGame :: Game.Context
  }

data Configuration = Configuration
  { confDir :: FilePath
  , confMessageLength :: Int
  }

main :: IO ()
main = do
  args <- getArgs
  rawMessage <- getRawMessage args
  gameContext <- Game.setup rawMessage
  let freq = Game.encryptedFrequency gameContext
  uiContext <- UI.setup $ maximum $ map (numberOfDigits . (fromIntegral :: Int -> Double)) $ A.elems freq
  _ <- catch
    ( evalStateT
      ( do
        drawEverything
        loop
      )
      Universe{uUI = uiContext, uGame = gameContext}
    )
    (\e -> do
      UI.endUI
      putStrLn "Error"
      print (e :: IOException)
    )
  UI.endUI

getRawMessage args = do
  conf0 <- getDefaultConf
  let conf = parseArgs conf0 args
  confExists <- doesDirectoryExist $ confDir conf
  when (not confExists) $ do
    setup conf
  let generator = (confDir conf) </> "generate"
  generateMessage generator (confMessageLength conf)

getDefaultConf = do
  dir <- getXdgDirectory XdgConfig "decrypt-your-fortune"
  return Configuration{confDir = dir, confMessageLength = 500}

generateMessage generator len = do
  msg <- nextMessage generator
  let len' = len - (length $ filter isAlpha msg)
  msgs <- if len' > 0 then
            generateMessage generator len'
          else
            return []
  return $ msg : msgs

nextMessage generator = do
  (_,Just sout, _,ph) <- createProcess $
    (proc generator [])
      { std_out = CreatePipe
      }
  outStr <- hGetContents sout
  _ <- waitForProcess ph
  return outStr

setup :: Configuration -> IO ()
setup conf = do
  createDirectoryIfMissing True (confDir conf)
  let genFile = ((confDir conf) </> "generate")
  writeFile genFile defaultGenerator
  p <- getPermissions genFile
  setPermissions genFile p{executable = True}
  where
    defaultGenerator = "#!/usr/bin/env bash\n\
    \fortune\n"

parseArgs conf args = case args of
  ("--configuration-directory":dir:args') -> parseArgs conf{confDir = dir} args'
  ("-c":dir:args') -> parseArgs conf{confDir = dir} args'
  ("--message-length":len:args') -> parseArgs conf{confMessageLength = read len} args'
  ("-l":len:args') -> parseArgs conf{confMessageLength = read len} args'
  _ -> conf

loop :: StateT Universe IO ()
loop = do
  cmd <- withUI $ UI.getCommand
  universe <- get
  let encryptedClass = Game.encryptedFrequencyClass $ uGame universe
  case cmd of
    ChangePermutation fromC toC -> do
      let PermutationCypher cypher = Game.playerCypher $ uGame universe
          conflict = mhead $ filter (\c -> cypher A.! c == toC) (A.indices cypher)
      case conflict of
        Just conflictC -> do
          withGame $ Game.changePermutation conflictC (cypher A.! fromC)
          withUI $ UI.writeCypher conflictC (cypher A.! fromC) 
        Nothing -> return ()
      hasWon <- withGame $ do
        Game.changePermutation fromC toC
      withUI $ UI.writeCypher fromC toC 
      if hasWon then
        victory
      else do
        drawDecryptedText
        loop
    Hint -> do
      revealed  <- withGame $ Game.revealMostFrequent
      case revealed of
        Just (fromC, toC) -> do
          universe <- get
          withUI $ do
            UI.writeNumberOfHintsUsed (Game.numberOfHints $ uGame universe)
            UI.writeMessage (Pretty.pretty $ "Hint: " ++ [fromC] ++ " is mapped to " ++ [toC] ++ ".")
            UI.writeCypher fromC toC 
          drawDecryptedText
        Nothing -> return ()
      universe' <- get
      if (Game.incorrectAmount $ uGame universe') == 0 then
        victory
      else
        loop
    Resize -> do
      withUI $ do
        UI.resizeScreen
      drawEverything
      loop
    Quit -> return ()
    ScrollText s -> do
      withUI $ UI.scrollText s
      loop

drawEverything = do
  universe <- get
  let freq = Game.encryptedFrequency $ uGame universe
      encryptedText = Game.encryptedMessage $ uGame universe
      decryptedText = Game.decryptedMessage $ uGame universe
      encryptedClass = Game.encryptedFrequencyClass $ uGame universe
      PermutationCypher guessedCypher = Game.playerCypher $ uGame universe
      doubledLetters = M.keysSet $ M.filterWithKey (\c v -> v > (1 :: Int) && c /= '_') $
        foldr (\c m -> M.insertWith (+) c (1 :: Int) m) M.empty $ A.elems guessedCypher
  withUI $ do
    forM_ (A.assocs freq) $ \(c,f) -> do
      UI.writeFrequency c (show f)
    forM_ (A.assocs guessedCypher) $ \(fromC, toC) -> do
      UI.writeCypher fromC toC 
    UI.writeParagraphs $ prettyMessage encryptedText decryptedText doubledLetters encryptedClass
    UI.writeInfo $ prettyInfo 
                     (Game.frequentSubstrings $ uGame universe)
                     (Game.frequentDecryptedSubstrings $ uGame universe)
                     doubledLetters
                     encryptedClass

prettyInfo encStrings decStrings doubledLetters encryptedClass = 
  foldr (\p q -> p Pretty.<> Pretty.softline Pretty.<> q) Pretty.emptyDoc $
    zipWith
      (\(e, c) d -> 
        (Pretty.annotate (UI.Single (UI.Encrypted Neutral)) $ Pretty.pretty $ show c ++ ":")
        Pretty.<>
        ( foldr (\(cy, dec) doc ->
            Pretty.annotate (UI.Multi (UI.Encrypted $ encryptedClass A.! cy) [(T.pack [dec], UI.Decrypted)])
              (Pretty.pretty cy) Pretty.<> doc
           )
           Pretty.emptyDoc
           (zip e d)
        )
      )
      encStrings
      decStrings

prettyMessage encryptedText decryptedText doubledLetters encryptedClass =
  foldr (\p q -> p
                 Pretty.<> Pretty.hardline
                 Pretty.<> Pretty.hardline
                 Pretty.<> q)
        Pretty.emptyDoc $
    zipWith 
      (\p i -> 
        (Pretty.annotate (UI.Single (UI.Encrypted Neutral)) $ Pretty.pretty (show i ++ "."))
        Pretty.<+>
        (Pretty.align p
        )
      )
      paragraphs
      [1 :: Int ..]
  where
    paragraphs = zipWith (mergeBlocks doubledLetters encryptedClass) encryptedText decryptedText
 
mergeBlocks doubledLetters encryptedClass encStr decStr = evalState (merge') (encStr, decStr)
  where
    whenNotEmpty def action = do
      (enc,_) <- get
      if null enc then
        return def
      else
        action
    nextDoubled = do
      (enc, dec) <- get
      let (d, rs) = span2 (`S.member` doubledLetters) enc dec
      put rs
      return d
    nextOK = do
      (enc, dec) <- get
      let (d, rs) = span2 (not . (`S.member` doubledLetters)) enc dec
      put rs
      return d
    span2 _ [] _ = (([],[]), ([],[]))
    span2 _ _ [] = (([],[]), ([],[]))
    span2 p (a:as) (b:bs)
      | p b = 
        let ((xs, ys), (as', bs')) = span2 p as bs
        in  ((a:xs, b:ys), (as', bs'))
      | otherwise = (([], []), (a:as, b:bs))
    merge' = 
      whenNotEmpty (Pretty.emptyDoc) $ do
        (encD, decD) <- nextDoubled
        (encOK, decOK) <- nextOK
        let encDDoc  = foldr (\(c, dec) d ->
                                let cls = encryptedClass A.! c
                                in
                                Pretty.annotate (UI.Multi (UI.Encrypted cls) [(T.pack [dec], UI.Wrong)]) $
                                  Pretty.pretty [c]
                                  Pretty.<>
                                  Pretty.softline'
                                  Pretty.<>
                                  d
                                )
                             Pretty.emptyDoc
                             (zip encD decD)
            encOKDoc = foldr (\(c, dec) d ->
                                let cls = encryptedClass A.! c
                                in
                                Pretty.annotate (UI.Multi (UI.Encrypted cls) [(T.pack [dec], UI.Decrypted)]) $
                                   Pretty.pretty [c]
                                   Pretty.<>
                                   Pretty.softline'
                                   Pretty.<>
                                   d
                                  )
                             Pretty.emptyDoc
                             (zip encOK decOK)
            doc = encDDoc Pretty.<> encOKDoc
        doc' <- merge'
        return $ doc Pretty.<> doc'

drawDecryptedText = do
  universe <- get
  let decryptedText = Game.decryptedMessage $ uGame universe
      encryptedText = Game.encryptedMessage $ uGame universe
      encryptedClass = Game.encryptedFrequencyClass $ uGame universe
      PermutationCypher playerCypher = Game.playerCypher $ uGame universe
      doubledLetters = M.keysSet $ M.filterWithKey (\c v -> v > (1 :: Int) && c /= '_') $
        foldr (\c m -> M.insertWith (+) c (1 :: Int) m) M.empty $ A.elems playerCypher
  withUI $ do
    UI.writeParagraphs $ prettyMessage encryptedText decryptedText doubledLetters encryptedClass
    UI.writeInfo $ prettyInfo 
                     (Game.frequentSubstrings $ uGame universe)
                     (Game.frequentDecryptedSubstrings $ uGame universe)
                     doubledLetters
                     encryptedClass

prettyEncrypted :: String -> Pretty.Doc UI.LineStyle
prettyEncrypted str = foldr 
  (\c txt -> Pretty.pretty [c] Pretty.<> Pretty.softline' Pretty.<> txt)
  Pretty.emptyDoc
  str

prettyDecrypted :: S.Set Char -> String -> Pretty.Doc UI.LineStyle
prettyDecrypted  wrong str = Pretty.annotate (UI.Single UI.Decrypted) $ foldr 
  (\c txt -> (prettyChar c) Pretty.<> Pretty.softline' Pretty.<> txt)
  Pretty.emptyDoc
  str
  where
    prettyChar c
      | c `S.member` wrong = Pretty.annotate (UI.Single UI.Wrong) $ Pretty.pretty [c]
      | otherwise = Pretty.pretty [c]

prettyOriginal :: String -> Pretty.Doc UI.LineStyle
prettyOriginal str =
  (Pretty.align $ foldr (\a b -> a <> Pretty.hardline <> b) Pretty.emptyDoc $ map prettyLine $ lines str)

prettyLine str = foldr (\a b -> a Pretty.<> Pretty.softline Pretty.<> b)
                       (Pretty.pretty "") $ 
                        map Pretty.pretty $ words str

withUI :: (StateT UI.Context IO a) -> StateT Universe IO a
withUI f = do
  universe <- get
  liftIO $ evalStateT f (uUI universe)

withGame f = do
  universe <- get
  let (ret, gameCx) = runState f (uGame universe)
  put universe{uGame = gameCx}
  return ret

victory = do
  universe <- get
  withUI $ do
    UI.writeMessage $ Pretty.pretty "Correct!"
    liftIO $ threadDelay (2 * (10^(6 :: Int) :: Int))
    UI.clearText
    UI.animateParagraphs
      (4 * (10^(4 :: Int) :: Int))
      $ foldr (\p q -> p
                       Pretty.<> Pretty.hardline
                       Pretty.<> q)
              Pretty.emptyDoc
              (map prettyOriginal $ Game.originalMessage $ uGame universe)
  _ <- withUI $ UI.getCommand
  return ()
      
  
--victory :: StateT Universe IO ()
--victory = do
--  drawVictory
--  liftIO $ threadDelay (2 * 10^6 :: Int)
--  clearText
--  drawUnencryptedMessage
--  getCommand >> return ()

--showContext cx = intercalate "\n"
--  [ intercalate "\n\n" $ cxEncryptedMessage cx
--  , "--------"
--  , decryptedStr
--  , "--------"
--  , ' ' : (intercalate "  " $ map (:[]) $ A.elems 
--      (let PermutationCypher cypher = cxGuessedCypher cx in cypher))
--  , ' ' : (intercalate "  " $ map (:[]) ['A'..'Z'])
--  , intercalate " "  $ map (\x -> show2 x) $ A.elems $ frequency (concat $ cxEncryptedMessage cx)
--  ]
--  where
--    decryptedStr = intercalate "\n\n" $ map (encrypt (cxGuessedCypher cx)) (cxEncryptedMessage cx)

